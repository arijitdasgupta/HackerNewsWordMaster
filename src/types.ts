
const TYPES = {
    ContentController: 'ContentController',
    ContentService: 'ContentService',
    CalculatorService: 'CalculatorService',
    HackerNewsRepository: 'HackerNewsRepository'
};

export { TYPES };