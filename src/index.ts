import * as express from 'express';
import * as expressLogger from 'express-logging';
import * as logger from 'logops';
import * as process from 'process';
import * as cors from 'cors';

import * as interfaces from './interfaces';
import { container } from './inversify.config';
import { TYPES } from './types';
import { NodePort } from './env';

const app = express();

app.set('port', NodePort);

app.use(expressLogger(logger));
app.use(cors({origin: true}));

const initApplication = () => {
    app.listen(app.get('port'), () => {
        console.log(`Application is listening on PORT ${app.get('port')}`)
    });
};

const contenController = container.get<interfaces.IController>(TYPES.ContentController);

app.use('/words', contenController.application);

// Just a status endpoint
app.get('/', (request:express.Request, response:express.Response) => {
    response.send('OK');
});

initApplication();




