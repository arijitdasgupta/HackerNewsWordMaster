import * as express from 'express';

export interface IController {
    application: express.Application;
}