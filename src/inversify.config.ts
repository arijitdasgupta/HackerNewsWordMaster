import { Container } from "inversify";
import * as inversify from "inversify";
import "reflect-metadata";

import { TYPES } from "./types";
import  * as appInterfaces from "./interfaces";
import { ContentController } from "./controllers/ContentController";
import { ContentService } from "./services/ContentService";
import { CalculatorService } from "./services/CalculatorService";
import { HackerNewsRepository } from "./repositories/HackerNewsRepository";
import * as env from './env';

const container = new Container();

container.bind<ContentService>(TYPES.ContentService).to(ContentService);
container.bind<HackerNewsRepository>(TYPES.HackerNewsRepository).to(HackerNewsRepository);
container.bind<CalculatorService>(TYPES.CalculatorService).to(CalculatorService);
container.bind<appInterfaces.IController>(TYPES.ContentController).to(ContentController).inSingletonScope();

export { container };