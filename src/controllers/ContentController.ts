import * as express from 'express';
import { TYPES } from '../types';
import { inject, injectable } from 'inversify';

import * as interfaces from '../interfaces';
import { ContentService } from '../services/ContentService';
import * as _ from 'lodash';

@injectable()
export class ContentController implements interfaces.IController {
    private contentService: ContentService;
    application: express.Application;

    defaultWordLength = 4;
    defaultStoryCount = 25;
    defaultWordCount = 10;
    defaultMinKarma = 10000;

    constructor(@inject(TYPES.ContentService) contentService:ContentService) {
        this.contentService = contentService;

        this.application = express();

        this.application.get('/lateststories', this.getLatest);
        this.application.get('/weekstories', this.getLastWeek);
        this.application.get('/topkarma', this.getTopKarmaStories);
    }

    private nanCheck = (n:string, deflt:number):number => {
        const parsed = parseInt(n, 10);
        return !_.isNaN(parsed) ? parsed : deflt;
    }

    private getLatest:express.RequestHandler = async (request, response) => {
        const wordCount = this.nanCheck(request.query.words, this.defaultWordLength);
        const storyCount = this.nanCheck(request.query.stories, this.defaultStoryCount);
        const minWordLength = this.nanCheck(request.query.wordlen, this.defaultWordLength);

        try {
            const res = await this.contentService.getTopWordsFromLatestStories(wordCount, storyCount, minWordLength);
            response.send(res);
        } catch(e) {
            response.status(500).send('ERROR');
        }


        
    }
    
    private getLastWeek:express.RequestHandler = async (request, response) => {
        const wordCount = this.nanCheck(request.query.words, this.defaultWordCount);
        const minWordLength = this.nanCheck(request.query.wordlen, this.defaultWordLength);

        response.send(await this.contentService.getTopWordsFromLastWeek(wordCount, minWordLength));
    }

    private getTopKarmaStories:express.RequestHandler = async (request, response) => {
        const wordCount = this.nanCheck(request.query.words, this.defaultWordCount);
        const minKarma = this.nanCheck(request.query.minkarma, this.defaultMinKarma);
        const minWordLength = this.nanCheck(request.query.wordlen, this.defaultWordLength);

        response.send(await this.contentService.getTopWordsOverKarma(wordCount, minKarma, minWordLength));
    }
}