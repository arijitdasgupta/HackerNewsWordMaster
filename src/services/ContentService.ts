import { injectable, inject } from 'inversify';
import * as _ from 'lodash';

import { TYPES } from '../types';
import { HackerNewsRepository } from '../repositories/HackerNewsRepository';
import { CalculatorService, ITopWords } from './CalculatorService';

export interface IDataReturnType {
    topWords: ITopWords[];
}

@injectable()
export class ContentService {
    hackerNewsRepository: HackerNewsRepository;
    calculatorService: CalculatorService;

    constructor(@inject(TYPES.HackerNewsRepository) hackerNewsRepository:HackerNewsRepository,
                @inject(TYPES.CalculatorService) calculatorService:CalculatorService) {
        this.hackerNewsRepository = hackerNewsRepository;
        this.calculatorService = calculatorService;
    }

    getTopWordsFromLatestStories = async (wordCount: number, storyCount: number, minWordLength: number):Promise<IDataReturnType> => {
        const storyIds = await this.hackerNewsRepository.getLatestStoryList();
        const latest = storyIds.slice(0, storyCount + 1);
        
        const topWords = await this.calculatorService.getTopWords(latest, wordCount, minWordLength);

        return {
            topWords
        };
    }

    getTopWordsFromLastWeek = async (wordCount: number, minWordLength: number):Promise<IDataReturnType> => {
        const storyIds = await this.hackerNewsRepository.getLatestStoryList();
        const topWords = await this.calculatorService.getWordsOfTheWeek(storyIds, wordCount, minWordLength);

        return {
            topWords
        }
    }

    getTopWordsOverKarma = async (wordCount: number, minKarma: number, minWordLength: number):Promise<IDataReturnType> => {
        const storyIds = await this.hackerNewsRepository.getLatestStoryList();
        const topWords = await this.calculatorService.getWordsWithMinKarma(storyIds, minKarma, wordCount, minWordLength);

        return {
            topWords
        }
    }
}