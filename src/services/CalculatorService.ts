import { injectable, inject } from 'inversify';
import * as Rx from 'rxjs/Rx';
import * as moment from 'moment';

import { TYPES } from '../types';
import { HackerNewsRepository } from '../repositories/HackerNewsRepository';

export interface ITopWords {
    word: string;
    count: number;
}

@injectable()
export class CalculatorService {
    hackerNewsRepository:HackerNewsRepository;

    constructor(@inject(TYPES.HackerNewsRepository) hackerNewsRepository:HackerNewsRepository) {
        this.hackerNewsRepository = hackerNewsRepository;
    }

    /**
     * Takes story IDs and shoots out a stream of stories, retries once if fails
     */
    private getStoriesRx = (storyIds: number[]):Rx.Observable<any> => {
        return Rx.Observable.from(storyIds)
            .flatMap(storyId => this.hackerNewsRepository.getStory(storyId))
            .retry(1) // Retry once...
            .catch(_ => Rx.Observable.of(null))
            .filter(item => !!item)
    }

    /**
     * Takes stream of hacker news stories, and spits out a stream of words
     */
    private wordsRx = (stream: Rx.Observable<any>, minWordLength:number):Rx.Observable<string> => {
        return stream.map(storyItem => (typeof storyItem.title === 'string') ? storyItem.title : '')
            .flatMap(title => Rx.Observable.from(title.split(/\W/)))
            .map((word:string) => word.replace(/\.$/, '')) // Removing `.` from the end of the words
            .filter((word:string) => !!word.replace(/\W/, '').length)
            .filter((word:string) => word.length >= minWordLength)
    }

    /**
     * Takes stream and filters to last week's stories
     */
    private filterToWeek = (stream: Rx.Observable<any>):Rx.Observable<any> => {
        return stream.filter(story => !!story.time)
            .filter(story => story.time > moment().subtract(7, 'days').unix())
    }

    /**
     * Checks authors, and their karma and it more than minKarma, then pass the story on
     */
    private filterToKarma = (stream: Rx.Observable<any>, minKarma: number):Rx.Observable<any> => {
        return stream.map(storyItem => ({
                author: storyItem.by ? storyItem.by : null,
                storyItem
            }))
            .filter(storyItemWithAuthor => storyItemWithAuthor.author) // Just a safety check
            .flatMap(storyItemWithAuthor => 
                this.hackerNewsRepository.getUser(storyItemWithAuthor.author).then(resp => ({
                    author: resp,
                    storyItem: storyItemWithAuthor.storyItem
                })))
            .retry(1) // Retrying one more time to get author
            .catch(e => Rx.Observable.of(null))
            .filter(i => i !== null) // catch the catched
            .filter(storyItemWithAuthorObject => 
                (storyItemWithAuthorObject.author && storyItemWithAuthorObject.author.karma) ? storyItemWithAuthorObject.author.karma : false)
            .filter(storyItemWithAuthorObject => storyItemWithAuthorObject.author.karma > minKarma) // the karma check
            .map(storyItemWithAuthorObject => storyItemWithAuthorObject.storyItem)
    }

    /**
     * Keeps count of words in `this` context, effective a hash
     * @param word 
     */
    private countWord(word:string) {
        if (this.hasOwnProperty(word)) {
            this[word] += 1;
        } else {
            this[word] = 1;
        }
    }

    /**
     * Calculates top words from a key:count hash
     */
    private calculateTopWords = (wordsHash:any, wordCount:number):ITopWords[] => {
        const wordsCountList = Object.keys(wordsHash).map(key => ({
            word: key,
            count: wordsHash[key]
        }));

        wordsCountList.sort((a, b) => b.count - a.count); // JavaScript built in sorting may be unstable, but for now... it works!
        return wordsCountList.slice(0, wordCount + 1)
    }

    /**
     * Creates a hashmap and subsribes to wrapes with resolve and reject
     */
    private resolveAndCount = (stream: Rx.Observable<string>, wordCount: number, resolve:(any) => void, reject:(any) => void) => {
        const wordsHashMap = {};
        
        stream.subscribe(word => this.countWord.bind(wordsHashMap)(word), 
            (e) => reject(e), 
            () => resolve(this.calculateTopWords(wordsHashMap, wordCount)));
    }

    public getTopWords = (storyIds: number[], wordCount: number, minWordLength: number):Promise<ITopWords[]> => {
        return new Promise((resolve, reject) => {
            const storyStream = this.getStoriesRx(storyIds)
            const words = this.wordsRx(storyStream, minWordLength)

            this.resolveAndCount(words, wordCount, resolve, reject);
        });
    }

    public getWordsOfTheWeek = (storyIds: number[], wordCount: number, minWordLength: number):Promise<ITopWords[]> => {
        return new Promise((resolve, reject) => {
            const storyStream = this.getStoriesRx(storyIds);
            const weekStream = this.filterToWeek(storyStream);
            const words = this.wordsRx(weekStream, minWordLength);

            this.resolveAndCount(words, wordCount, resolve, reject);
        });
    }

    public getWordsWithMinKarma = (storyIds: number[], minKarma: number, wordCount: number, minWordLength: number):Promise<ITopWords[]> => {
        return new Promise((resolve, reject) => {
            const storyStream = this.getStoriesRx(storyIds);
            const karmaFiltered = this.filterToKarma(storyStream, minKarma);
            const words = this.wordsRx(karmaFiltered, minWordLength);

            this.resolveAndCount(words, wordCount, resolve, reject);
        })
    }
}