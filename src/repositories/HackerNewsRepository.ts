import { injectable, inject } from 'inversify';
import { default as axios, AxiosInstance } from 'axios';

import { TYPES } from '../types';

@injectable()
export class HackerNewsRepository {
    private hackernewsApiUrl = `https://hacker-news.firebaseio.com/v0`;
    private axiosClient: AxiosInstance;

    constructor() {
        this.axiosClient = axios.create({
            baseURL: this.hackernewsApiUrl
        });
    }

    /**
     * Gets all the 600 latest stories
     */
    getLatestStoryList = ():Promise<number[]> => {
        return this.axiosClient.get('/newstories.json').then(resp => resp.data as number[]);
    }

    getUser = (userId:string):Promise<any> => {
        return this.axiosClient.get(`/user/${userId}.json`).then(resp => resp.data);
    }

    getStory = (storyId:number):Promise<any> => {
        return this.axiosClient.get(`/item/${storyId}.json`).then(resp => resp.data);
    }
}