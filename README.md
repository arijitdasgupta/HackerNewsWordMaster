HackerNewsWordMaster
====================

Top words and stuff...
---------------------

Gets top words from Hacker News story titles. Also it's quite limited to using the public API's `lateststories.json` endpoint.

This essentially is an experiment in RxJS for parallel heavy lifting in in lazy-mode with nodeJS. This application is NOT network usage efficient.

This application exposes three endpoints, 
```bash
/words/lateststories 
# Gets most occuring words from the latest stories Hacker News API exposes.

/words/topkarma 
# Gets most occuring words from the latest 600 stories while the auther has more than set karma points.

/words/weekstories 
# Gets most occuring words from the last week's stories, or last 600 stories, whichever comes first.
```

All these endpoints support two default query parameters (`word`, `wordlen`) and both of them can be used together...
```bash
/words/lateststories?wordlen=3 
# will limit the length of the words to minimum 3

/words/topkarma?words=42 
# will return top 42 words
```

Some endpoints have their special query parameters and can be combined with other query parameters

```bash
/words/lateststories?stories=35 
# Gets most occuring words on latest 35 stories
```

```bash
/words/topkarma?minkarma=20000 
# Gets most occuring words from latest 600 stories, if the auther has more than 20000 karma points
```
 
To run the application you would need `node`,`npm` installed in your system. Preferably node `> 8.0.0`

Development mode,
```bash
npm install
npm run watch
```

To build for production...

```bash
npm install
npm build
npm start # starts the application
```

This app is Heroku enabled.

You can try out the application on https://warm-harbor-93275.herokuapp.com. However it's a free heroku dyno, so it might be pretty slow, since Heroku de-activates dynos after a certain inactivity time.

NOTES:
 - No unit tests
 - Not network usage efficient
 - No caching of responses
 - Differentiates uppercase and lowercase letters when considering two words whether same or not
 - RxJS version shoots TypeScript compile warnings, harmless
 - No Swagger for API documentation
 - Does a lot of stuff in memory
